#!/bin/bash -eux

useradd -u 1000 -g users stagiaire
echo "stagiaire" | passwd --stdin stagiaire 

mkdir /home/GroupeA/
mkdir /home/GroupeB/
mkdir /home/GroupeP/

groupadd -g 500 GroupeA
groupadd -g 501 GroupeP

useradd -u 500 -g 500 -d /home/GroupeA/alain alain
useradd -u 501 -g 500 -d /home/GroupeA/albert albert
useradd -u 502 -g 501 -d /home/GroupeP/pascal pascal
useradd -u 503 -g 501 -d /home/GroupeP/patrick patrick

mkdir /STAGE
chgrp users /STAGE
chmod g+w /STAGE
