////
Les supports de Formatux sont publiés sous licence Creative Commons-BY-SA et sous licence Art Libre.
Vous êtes ainsi libre de copier, de diffuser et de transformer librement les œuvres dans le respect des droits de l’auteur.

    BY : Paternité. Vous devez citer le nom de l’auteur original.
    SA : Partage des Conditions Initiales à l’Identique.

Licence Creative Commons-BY-SA : https://creativecommons.org/licenses/by-sa/3.0/fr/
Licence Art Libre : http://artlibre.org/

Auteurs : Patrick Finet, Xavier Sauvignon, Antoine Le Morvan
////
= FORMATUX - Travaux Dirigés GNU/Linux : Administration CentOS 6 (Version corrigée)
:doctype: book
:toc!:
:docinfo:
:encoding: utf-8
:lang: fr
:numbered:
:sectnumlevels: 2
:description: Travaux Dirigés Linux de Formatux - Administration (Version corrigée)
:revnumber: 1.0
:revdate: 03-08-2017
:revremark: Version 1 
:version-label!: 
:experimental:
:source-highlighter: coderay
:checkedbox: pass:normal[+&#10004;]+]

include::0000-preface.adoc[]

=== Gestion des versions

.Historique des versions du document
[width="100%",options="header",cols="1,2,4"]
|====================
| Version | Date | Observations 
| 1.0 | Août 2017 | Version initiale.
|====================

include::FON-020-commandes-TD-corrige.adoc[leveloffset=+1]

include::FON-030-utilisateurs-TD-corrige.adoc[leveloffset=+1]

include::FON-040-systeme-de-fichiers-TD-corriges.adoc[leveloffset=+1]

include::FON-050-gestion-processus-TD-corriges.adoc[leveloffset=+1]

include::FON-060-sauvegardes-TD-corriges.adoc[leveloffset=+1]